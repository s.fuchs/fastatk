FASTA TOOLKIT (FASTATK) v3.0.1
=====================================================================

The FASTA TOOLKIT is a collection of useful functions to process and
modify FASTA file. It is written in Python3 by Stephan Fuchs (Robert 
Koch Institut, FG13, fuchss@rki.de).

The FASTA TOOLKIT is under active development and has been tested
under Ubuntu 16.04 and Debian jessy.

Importantly, the FASTA TOOLKIT provides a unittest-like routine to
validate its functionality (see section 4.6)


1 CONTENTS
=====================================================================
The FASTA TOOLKIT comes with two files:
    fastatk.py (the actual module/script)
    fastatk.testdata.fasta (a testfile for unittest)


2 INSTALLATION
=====================================================================
As long as there is not yet a package provided, copy the fastatk.py 
and fastatk.testdata.fasta files to a folder of your choice 
(path/to/folder/). Allow the execution of fastatk.py by running this 
command:
chmod u+x path/to/folder/fastatk.py


3 FASTA FORMAT
=====================================================================
The FASTA format considers three line types:
- head lines introduced by a greater-than character (>)
- comment lines introduced by a semicolon (;)
- sequence lines containing aminoacid or nucleotide sequences 

A valid FASTA entry start with a descriptive head line followed by 
one or more sequence lines. Although there is no convention about
the length of sequence lines, some programs have problems to process 
FASTA files containing very long sequence lines. Thus, a restriction 
of max 80 nucleotides/amino acids per sequence line can be useful. 
Comment lines are not frequently used and should be avoided.

A FASTA file can contain multiple entries while the head line of one
entry always marks the end of the previous entry. The head line can 
consist of a single header. 
Example:

>\>Protein_A
MTEKINKKDNYHLIFALIFLAIVSVVSMMIGSSFIPLQRVLMYFINPNDSMDQFTLEVLRLPRI
TLAILAGAALGMSGLMLQNVLKNPIASPDIIGITGGASLSAVVFIAFFSHLTIHLLPLFAVLGG
AVAMMILLVFQTKGQIRPTTLIIIGISMQTLFIALVQGLLITTKQLSAAKAYTWLVGSLYGATF
KDTIILGMVILAVVPLLFLVIPKMKISILDDPVAIGLGLHVQRMKLIQLITSTILVSMAISLVG
NIGFVGLIAPHIAKTIVRGSYAKKLLMSAMIGAISIVIADLIGRTLFLPKEVPAGVFIAAFGAP
FFIYLLLTVKKL

However, there are also head lines containing multiple header (e.g. 
in the NCBI nr database). In this case, different genes/proteins 
share the same sequence. To keep the sequences unique, all header 
are combined to a single head line. Control characters (separators) 
are used to mark the end of one header and the beginning of the
next header in the same head line. Usually the Start Of Heading
(SOH) character is used as a seperator (e.g. in the NCBI nr 
database). 
Example (• = SOH):

>\>Protein B•Protein C•Protein D
MTEKINKKDNYHLIFALIFLAIVSVVSMMIGSSFIPLQRVLMYFINPNDSMDQFTLEVLRLPRI
TLAILAGAALGMSGLMLQNVLKNPIASPDIIGITGGASLSAVVFIAFFSHLTIHLLPLFAVLGG
AVAMMILLVFQTKGQIRPTTLIIIGISMQTLFIALVQGLLITTKQLSAAKAYTWLVGSLYGATF
KDTIILGMVILAVVPLLFLVIPKMKISILDDPVAIGLGLHVQRMKLIQLITSTILVSMAISLVG
NIGFVGLIAPHIAKTIVRGSYAKKLLMSAMIGAISIVIADLIGRTLFLPKEVPAGVFIAAFGAP
FFIYLLLTVKKL

Actually, the SOH is a non-printable character and, thus, invisible.
However, some (but not all) editors use specific symbols 
(such as •) to indicate a SOH.
Unfortunately, not every program is able to interprete multiple 
header in one head line as different entries sharing the identical
sequence. Thus, multiple header should be only used, when 
downstream tools can handle them.

The FASTA Toolkit skips orphaned entries ("orphanes") which
consists only of a head line without any sequence information. 
These entries are not of any value and might indicate FASTA file 
corruption.


4 FUNCTIONS
=====================================================================
An description of all included functions can be assessed by running
this command:
`path/to/fastatk.py -h`

```
count               counts entries in one or more FASTA files.

extract             extracts entries of a FASTA file based on a list of
                        keywords/accessions.
                        
index               indexes FASTA entries and provides a indexed FASTA
                        file and index file.
                        
nr                  combines one or more FASTA files into a single file
                        and removes full-length sequence redundancy
                        
explode             splits FASTA entries introduced by multiple header in
                        one head line into single FASTA entries with single
                        header
                        
health              checks consistency of a given FASTA file

digest              creates a peptide file (FASTA) and, optionally, an
                        index file based on a given FASTA file
                        
unittest            performs an unittest-like routine to validate module
                        functionality
```                        


4.1 count
=====================================================================
This function can be used to count and show the number of entries, 
header, and orphaned entries (containing no sequence information) of 
one or more FASTA files. The number of entries and header can differ 
when multiple header are present (see section 3).

See description on how to use this function by running the command:
`path/to/fastatk.py count -h`

Following parameters are optional:

	-h, --help  show help message and exit


Following information are mandatory at the end of the count command:

    name(s) of one (mutliple) FASTA file(s)

Usage example:
`path/to/fastatk.py count file1.fasta file2.fasta`

Output:

```
processing file1.fasta ...
processing file2.fasta ...

FASTA file     entries   header  orphanes
file1.fasta      18283  7857515         0
file2.fasta      11423    11423         0
```


4.2 extract
=====================================================================
This function can be used to extract entries from a given FASTA file
based on keywords or accessions. The extraction is genuine, means
original lines are not modified during extraction and blank lines and
orphaned entries without sequence information are not filtered.

Entries can be selected by keywords/accessions present in the 
respective head line. There are two ways to restrict the headline 
information to keyword/accessions only.
First, one or different single characters can be defined wich are in 
front and after each keyword/accession.
 
For instance, UniRef accessions are encapsulated by space and 
underline characters. 
Example:
>UniRef100_Q6GZX3 Uncharacterized protein 002L n=1 Tax=Frog virus 3

The accession Q6GZX3 is introduced by a underline and followed by a 
space character.

In contrast, NCBI's protein accession numbers are encapsulated by 
greater-than and space characters. 
Example:
>WP_003131952.1 30S ribosomal protein S18 [Lactococcus lactis]

The accession WP_003131952.1 is introduced by a 
greater-than character and followed by a space.

Swiss-Prot, TrEMBL, and GI numbers are encapsulated by pipe chars
(|). 
Example:
>sp|Q2IGM4|FMT_ANADE Methionyl-tRNA formyltransferase

The accession Q2IGM4 is introduced and followed by a pipe (|) 
character.

It is important to note, that keywords do not contain any of
the encapsulating separators which is 
tested by the script and, in case, reported by an error message.

Alternatively, advanced users can use regular expression to define
the keyword/accession format. The first match group of this regular 
expression has to return the respective keywords/accessions.

See description on how to use the extract function by running the 
command:
`path/to/fastatk.py extract -h`

Following parameters are optional:
  ```
  -h, --help   	show help message and exit
  --name FILE  	name of the new FASTA file containing all extracted 
				entries. By default the output file is named 
				'extraction_from_fn' where fn is the name of the input 
				FASTA file.
  --enc STR    	specific characters encapsulating the keywords/
				accessions (joined to a string). By default 
				greater-than (>), space, tabulator, pipe (|), and 
				Seperator Of Header (SOH) chars are considered. 
				This option cannot be combined with --regex
  --regex STR  	regular expression capturing the keywords/accessions 
				in match group 1. This option cannot be 
				combined with --enc
  ```
                                
Following information are mandatory at the end of the count command:

        name of FASTA file (source)
        name plain text file containing keywords/accessions (one per 
		line).
                                
Usage example:
`path/to/folder/fastatk.py extract --enc ' _' --name extraction.fasta file1.fasta keywords.txt`

Output:
checking keywords/accessions ...
preparing output file...
extraction_from_UniRef100_2015_11.fasta
extracting ...
checking output ...
3753 entries were extracted based on 3753 unique keywords/accessions


4.3 nr
=====================================================================
This function can be used to remove full-length sequence redundancy 
in a single FASTA file or across multiple FASTA files. Header 
belonging to the identical sequence are fused to a single head line 
(see section 3). Non-redundant entries are saved to a new FASTA file.
Blank lines and orphaned entries (containing no sequence information)
are excluded.

See description on how to use this function by running the command:
`path/to/fastatk.py nr -h`

Following parameters are optional:

  -h, --help   	show help message and exit
  --name FILE  	name of the designated file storing the non-redundant 
				FASTA entries (by default: nr.fasta)
  -l INT       	maximal length of sequence lines in the generated 
				non-redundant FASTA file (by default: 80). If set 
				to 0 or less, sequence lines will be not wrapped.

                                        
Following information are mandatory at the end of the extract command:

        name of FASTA file(s)
        
Usage example:
`path/to/fastatk.py nr file1.fasta file2.fasta`
        
```
Output:
processing short_nr ...
processing short_uni ...
sorting ...
writing ...
```


4.4 explode
=====================================================================
This function can be used to split headlines with multiple header 
(see section 3) into single entries. 
Blank lines and orphaned entries (containing no sequence information)
are excluded.

See description on how to use this function by running the command:
`path/to/fastatk.py explode -h`

Following parameters are optional:

  -h, --help   	show help message and exit
  --name FILE  	name of the designated file (by default: 
				redundant.fasta)
  -l INT       	maximal length of sequence lines in the generated 
				non-redundant FASTA file (by default: 80). If set 
				to 0 or less, sequence lines will be not wrapped.
                                 
Following information are mandatory at the end of the extract command:

        name of FASTA file
        
Usage example:
`path/to/fastatk.py explode file.fasta`
        
Output:
```
processing file.fasta  ...
1006 entries splitted into 4323 entries, 1 orphane(s) skipped
```


4.5 health
=====================================================================
This function can be used to check the consistency of a given FASTA file.

See description on how to use this function by running the command:
`path/to/fastatk.py health -h`

Following parameters are optional:

  -h, --help   	    show help message and exit
  --name FILE  	    name of the log file to be created (by default: 
				    fasta_health.log)
  --type STR        sequence type that can be dna, rna or aa (default: aa).
  --headerfmt STR   regular expression matching to valid FASTA headers (by
                    default: >\S.*)
                                 
Following information are mandatory at the end of the extract command:

        name of FASTA file
        
Usage example:
`path/to/fastatk.py health file.fasta`
        
Output:
```
processing file.fasta ...
checking entries ...
checking duplicates ...
entries (without orphanes): 9914
header (without orphanes): 9914
orphanes: 0
sequences: 9914
unique sequences: 9909
entries with non-valid header: 0
sequences containing illegal characters: 4
sequences containing ambiguous characters: 1401
sequences containing gaps: 0
header assigned to same sequence multiple times: 0
header assigned to multiple sequences: 0
for more details see fasta_health.log
```


4.6 digest
=====================================================================
This function can be used to create a peptide FASTA file. For this,
all sequences in a given FASTA file a digested in silico. In general,
the shortest possible peptides are generated. However, number of 
possible miscleavages can be defined. Resulting peptides are stored 
in a new FASTA file. Peptides coordinates related to the original 
sequence are added to each peptide header. Optionally, redundant 
peptides can be combined to one entry with a multiple headers (see 
section 3). Moreover, the generated FASTA file can be indexed meaning 
that the entries are numbered and an index file listing indices and 
the corresponding headers (with coordinates) is generated. Blank 
lines and orphaned entries (containing no sequence information) are 
excluded.

Following proteases are supported:

trypsin			cuts after Arg or Lys if not followed by Pro

chymotrypsin	cuts after Trp, Tyr, Phe or Leu

lys-c			cuts after Lys if not followed by Pro

arg-c			cuts after Arg if not followed by Pro or 
				after Lys-Lys
				
glu-c			cuts after Glu or Asp

asp-n			cuts before Asp or Cys

See description on how to use the explode function by running the 
command:
`path/to/fastatk.py explode -h`

Following parameters are optional:

  -h, --help            show help message and exit
  --name FILE           name of the designated file containing all 
						peptides in FASTA format (by default: 
						peptide.fasta)
  --protease {trypsin,lys-c,arg-c,glu-c,asp-n,chymotrypsin}
						in silico protease (default: trypsin)
  --mc INT              number of allowed miscleavages (default: 0)
  --minlen INT         	minimal length of a peptide to be considered 
						(default: 5)
  --nr                  combines same peptides in a single FASTA 
						entry with a multiple header
  --index               index sequences and generate an index file
						combining indices with original headers
  --oneline             use one line format for index file (multiple
						header will be fused using semicolon as 
						separator)
  --prefix INT          prefix used for peptide indices (default: pep_)
  -l INT                maximal length of sequence lines in the generated
                        FASTA file (by default: 80). If set to 0 or less,
                        sequence lines will be linear and not wrapped.
       
Following information are mandatory at the end of the extract command:

        name of FASTA file
        
Usage example:
`path/to/fastatk.py digest file.fasta`
        
Output:
```
processing file.fasta  ...
```


4.7 unittest
=====================================================================
This function can be used to check functionality of this module in
your environment. No parameters are allowed.

After running the test routine, the number of failed and succeeded 
tests are shown. Thumbs up!

Usage example:
`path/to/fastatk.py unittest`

Output if you are lucky ;)
```
TestResults(failed=0, attempted=78)
```

For a more detailed test report please run this command:
`python3 -m doctest -v path/to/folder/fastatk.py`


5 DISCLAIMER
=====================================================================
For redistributing and/or modifying please contact Stephan Fuchs 
(fuchss@rki.de).

This program programmed and tested very carefully, however, for legal 
reasons no liability can be assumed for any generated information.

copyright (c) 2018 Stephan Fuchs (fuchss@rki.de)


6 FEEDBACK
=====================================================================
Have fun and provide feedback to
fuchss@rki.de
