#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#this module has been developed by Stephan Fuchs (Robert Kocj Institute, FG13, fuchss@rki.de)



#dependencies
import argparse
from contextlib import ExitStack
from collections import defaultdict
import doctest
from heapq import merge
from itertools import count, islice, chain
import os
import shutil
import sys
import time
import re
import tempfile


#ouput class
class output():
	'''
	creates, modifies, and deletes files & folder
	output stores tuples (rel name, abs name, dir|file)
	'''

	@staticmethod
	def ask_user(question, yes= "y", no="n", autoyes=False, sense_case=False):
		'''
		user prompt returns True (user confirmed) or False (user did not confirm)
		question	quatsion to prompt
		yes			confirmation input
		no			decline input
		autoyes		skip prompt and return True	(True/False)
		sense_case	case-sensitivity (True/False)
		'''

		if autoyes:
			return True
		while True:
			prompt = input(question + " [" + str(yes) + "/" + str(no) + "] ")
			if not sense_case:
				prompt = prompt.lower()
				yes = yes.lower()
				no = no.lower()
			if prompt == yes:
				return True
			elif prompt == no:
				return False

	@staticmethod
	def createDir(dirname, addtimestmp = False, owr = False, files=[]):
		'''
		creates a folder and returns folder name
		dirname			name of folder to create
		addtimestmp 	if True, a timestamp "%Y-%m-%d_%H-%M-%S" is added to the folder name
		owr				if True, defined files in existing folders are overwritten (deleted) without user confirmation
		files			list of files user should be asked for overwriting
		'''

		#dir with timestamp
		if addtimestmp:
			dirpref = dirname
			err = 0
			while True:
				timecode = time.strftime("%Y-%m-%d_%H-%M-%S")
				dirname = dirpref + '_' + timecode
				if os.path.isdir(dirname):
					err += 1
				else:
					break
				if err == 30:
					sys.exit("ERROR: dirnames existent (30 timestamps checked")
			os.makedirs(dirname)

		#existing dir
		if os.path.isdir(dirname):
			files = [os.path.join(dirname, os.path.basename(x)) for x in files]
			output.check_files(files)

		#non-existing dir
		else:
			os.makedirs(dirname)

		return dirname

	@staticmethod
	def check_file(*argv):
		existing_files = [x for x in argv if os.path.isfile(x)]
		if existing_files:
			print("WARNING: following file(s) will be overwritten: " + ", ".join([os.path.basename(x) for x in existing_files]))
			if not output.ask_user("Continue?"):
				sys.exit("terminated on user request")
			for existing_file in existing_files:
				os.remove(existing_file)
		return True

	@staticmethod
	def check_in_out(*argv):
		if len(argv) != len(set(argv)):
			sys.exit("ERROR: ouput is input.")
		return True


#fastatk class
class fastatk():
	### Initialization ###
	def __init__():
		pass


	### Basic checks and formatting ###

	@staticmethod
	def isHeader(line):
		"""
		checks if line is a Fasta header and returns True or False. Line is not stripped.

		>>> fastatk.isHeader('>header1\\n')
		True
		>>> fastatk.isHeader(' >header2\\n')
		False
		>>> fastatk.isHeader('ATGCAATTCATGGGTC\\r\\n')
		False
		>>> fastatk.isHeader(' \\t\\r\\n')
		False
		>>> fastatk.isHeader(';comment1\\n')
		False

		"""

		if len(line) > 0 and line[0] == ">":
			return True
		else:
			return False


	@staticmethod
	def isBlank(line):
		"""
			checks if stripped line is blank and returns True or False.

			>>> fastatk.isBlank('>header1\\n')
			False
			>>> fastatk.isBlank(' >header2\\n')
			False
			>>> fastatk.isBlank('ATGCAATTCATGGGTC\\r\\n')
			False
			>>> fastatk.isBlank(' \\t\\r\\n')
			True
			>>> fastatk.isBlank(';comment1\\n')
			False

		"""

		if len(line.strip()) == 0:
			return True
		else:
			return False


	@staticmethod
	def isComment(line):
		"""
			checks if line is comment and returns True or False. Line is not stripped

			>>> fastatk.isComment('>header1\\n')
			False
			>>> fastatk.isComment('ATGCAATTCATGGGTC\\r\\n')
			False
			>>> fastatk.isComment(' \\t\\r\\n')
			False
			>>> fastatk.isComment(';comment1\\n')
			True
			>>> fastatk.isComment(' ;comment2\\n')
			False

		"""

		if len(line) > 0 and line[0] == ";":
			return True
		else:
			return False


	@staticmethod
	def breakSeq(seq, n = 80, lb = '\n'):
		"""
		Introduces a line break (default: \n) every nth caharacter.

		>>> fastatk.breakSeq('ATGCAATTCATGGGTCCGTAAATCGATCTAGGC', 5)
		'ATGCA\\nATTCA\\nTGGGT\\nCCGTA\\nAATCG\\nATCTA\\nGGC'
		>>> fastatk.breakSeq('ATGCAATTCATGGGTCCGTAAATCGATCTAGGC', 5, '\\r\\n')
		'ATGCA\\r\\nATTCA\\r\\nTGGGT\\r\\nCCGTA\\r\\nAATCG\\r\\nATCTA\\r\\nGGC'

		"""
		if not n or len(seq) <= n:
			return seq
		return lb.join([ seq[i:i+n] for i in range(0, len(seq), n) ])


	### file processing ###

	@staticmethod
	def readFasta(filename, silent = False):
		'''
		Creates a generator returning tuples of (header, sequence) obtained from a FASTA file. Lines are stripped.

		>>> testfile = os.path.join(os.path.dirname(__file__), 'fastatk.testdata.fasta')
		>>> entries = 0
		>>> chars = 0
		>>> for header, seq in fastatk.readFasta(testfile, silent=True):
		...	entries += 1
		...	chars += len(header) + len(seq)
		>>> (entries, chars)
		(7, 2273)

		#SHELL way to get testdata attributes for doctest:
		#entries: grep '^>' fastatk.testdata1 | wc -l
		#chars: sed 's/^[ \t]*//' fastatk.testdata1 | sed 's/[ \t]*$//' | sed 's/^;.*//' | tr -d '\n' | tr -d '\r' | wc -m

		'''

		#line-by-line processing
		with open(filename, "r") as fhandle:
			##msg
			if not silent:
				print("processing", fhandle.name, "...")

			#find first header
			for line in fhandle:
				line = line.strip()

				if fastatk.isHeader(line):
					header = line
					break

			#process file after first header
			seq = []
			for line in fhandle:
				line = line.strip()

				if fastatk.isComment(line) or fastatk.isBlank(line):
					continue

				elif fastatk.isHeader(line):
					entry = (header, "".join(seq))
					header = line
					seq = []
					yield entry

				else:
					seq.append(line)

			entry = (header, "".join(seq))
			yield entry

	### FASTA attributes ###

	# count Entries #
	@staticmethod
	def countEntries(filename, soh = chr(1), silent = False):
		'''
		count entries in Fasta File	and returns tuple (number of entries excluding oprhanes, number of header excluding orphanes, number of orphanes).
		Number of header and entries can differ when multiple headers are used (soh is set).
		soh		separator of header

		>>> #let's get the test data file first
		>>> testfile = os.path.join(os.path.dirname(__file__), 'fastatk.testdata.fasta')
		>>> #test it
		>>> fastatk.countEntries(testfile,silent = True)
		(6, 9, 1)

		'''

		entries = 0
		header = 0
		orphanes = 0

		#create file for sorting
		for h, s in fastatk.readFasta(filename, silent = silent):
			if not s:
				orphanes += 1
			else:
				entries += 1
				if soh:
					header += len(h.split(soh))

		if not soh:
			header = entries

		return entries, header, orphanes


	### FASTA transformations ###

	## extract entries ##
	@staticmethod
	def extractEntries(keywords, enclosures, infilename, outfilename = None, regex = False, silent = False):
		"""
		Performs keyword/accession-based extraction of FASTA entries out of a given file to a new file. Extraction is genuine, means lines are not stripped and blank lines, comments, orphanes, and multiple headers are not excluded from extraction.
		If outfilename is given number of extracted entries is returned.
		If outfilename is not given (None) string of all extracted lines is returned.
		Warnings of keywords not found or found mutiple times are written to STDERR.
		if regex is True a regex is expected eas sepstr
		enclosures	are chars enclosing the accession OR regex which as the accession as 1st group!

		>>> testfile = os.path.join(os.path.dirname(__file__), 'fastatk.testdata.fasta')
		>>> accessions = ['ACC2', 'ACC6', 'ACC7', 'ACC8', 'ACC8', 'ACC10']
		>>> fastatk.extractEntries(accessions, '> _', infilename=testfile, silent=True)
		'>DB_ACC1 protein A\\x01DB_ACC2 protein A\\x01DB_ACC4 protein A\\x01DB_ACC4 protein A\\nMTEKINKKDNYHLIFALIFLAIVSVVSMMIGSSFIPLQRVLMYFINPNDSMDQFTLEVLRLPRITLAILAGAALGMSGLM\\nLQNVLKNPIASPDIIGITGGASLSAVVFIAFFSHLTIHLLPLFAVLGGAVAMMILLVFQTKGQIRPTTLIIIGISMQTLF\\nIALVQGLLITTKQLSAAKAYTWLVGSLYGATFKDTIILGMVILAVVPLLFLVIPKMKISILDDPVAIGLGLHVQRMKLIQ\\nLITSTILVSMAISLVGNIGFVGLIAPHIAKTIVRGSYAKKLLMSAMIGAISIVIADLIGRTLFLPKEVPAGVFIAAFGAP\\nFFIYLLLTVKKL\\n>DB_ACC6 protein B (entry with blank line after)\\nMRYTVLIALQGALLLLLLIDDGQGQSPYPYPGMPCNSSRQCGLGTCVHSRCAHCSSDGTLCSPEDPTMVWPCCPESSCQL\\nVVGLPSLVNHYNCLPNQCTDSSQCPGGFGCMTRRSKCELCKADGEACNSPYLDWRKDKECCSGYCHTEARGLEGVCIDPK\\nKIFCTPKNPWQLAPYPPSYHQPTTLRPPTSLYDSWLMSGFLVKSTTAPSTQEEEDDY\\n\\n>DB_ACC7 orphaned entry\\n>DB_ACC8 protein C (entry with comment)\\n;comment to DB_ACC8\\nMNIELLQQLCEASAVSGDEQEVRDILINTLEPCVNEITFDGLGSFVARKGNKGPKVAVVGHMDEVGFMVTHIDESGFLRF\\nTTIGGWWNQSMLNHRVTIRTHKGVKIPGVIGSVAPHALTEKQKQQPLSFDEMFIDIGANSREEVEKRGVEIGNFISPEAN\\nFACWGEDKVVGKALDNRIGCAMMAELLQTVNNPEITLYGVGSVEEEVGLRGAQTSAEHIKPDVVIVLDTAVAGDVPGIDN\\nIKYPLKLGQGPGLMLFDKRYFPNQKLVAALKSCAAHNDLPLQFSTMKTGATDGGRYNVMGGGRPVVALCLPTRYLHANSG\\nMISKADYEALLTLIRGFLTTLTAEKVNAFSQFRQVD\\n>DB_ACC10 protein D (last entry)\\nMESMLDRPEQELVLVVDDTPDNLLLMRELLEEQYRVRTAGSGPAGLRAAVEEPRPDLILLDVNMPGMDGYEVCRRLKADP\\nLTRDIPLMFLTARADRDDEQQGLALGAVDYLGKPVSPPIVLARVRTHLQLKANADFLRDKSEYLELEVRRRTRQLQQLQD\\nAVIEALATLGDLRDNPRSRHLPRIERYVRLLAEHLAAQRAFADELTPEAVDLLSKSALLHDIGKVAVPDRVLLNPGQLDA\\nADTALLQGHTRAGRDALASAERRLGQPSGFLRFARQIAYSHHERWDGRGFPEGLAGERIPLAARIVALADRYDELTSRHA\\nYRPPLAHAEAVLLIQAGAGSEFDPRLVEAFVAVADAFAEVARRYADSAEALDVEMQRLEQAVAESIELTAPPA'
		>>> fastatk.extractEntries(accessions, '_([^\s]+)', infilename=testfile, regex=True, silent=True)
		'>DB_ACC1 protein A\\x01DB_ACC2 protein A\\x01DB_ACC4 protein A\\x01DB_ACC4 protein A\\nMTEKINKKDNYHLIFALIFLAIVSVVSMMIGSSFIPLQRVLMYFINPNDSMDQFTLEVLRLPRITLAILAGAALGMSGLM\\nLQNVLKNPIASPDIIGITGGASLSAVVFIAFFSHLTIHLLPLFAVLGGAVAMMILLVFQTKGQIRPTTLIIIGISMQTLF\\nIALVQGLLITTKQLSAAKAYTWLVGSLYGATFKDTIILGMVILAVVPLLFLVIPKMKISILDDPVAIGLGLHVQRMKLIQ\\nLITSTILVSMAISLVGNIGFVGLIAPHIAKTIVRGSYAKKLLMSAMIGAISIVIADLIGRTLFLPKEVPAGVFIAAFGAP\\nFFIYLLLTVKKL\\n>DB_ACC6 protein B (entry with blank line after)\\nMRYTVLIALQGALLLLLLIDDGQGQSPYPYPGMPCNSSRQCGLGTCVHSRCAHCSSDGTLCSPEDPTMVWPCCPESSCQL\\nVVGLPSLVNHYNCLPNQCTDSSQCPGGFGCMTRRSKCELCKADGEACNSPYLDWRKDKECCSGYCHTEARGLEGVCIDPK\\nKIFCTPKNPWQLAPYPPSYHQPTTLRPPTSLYDSWLMSGFLVKSTTAPSTQEEEDDY\\n\\n>DB_ACC7 orphaned entry\\n>DB_ACC8 protein C (entry with comment)\\n;comment to DB_ACC8\\nMNIELLQQLCEASAVSGDEQEVRDILINTLEPCVNEITFDGLGSFVARKGNKGPKVAVVGHMDEVGFMVTHIDESGFLRF\\nTTIGGWWNQSMLNHRVTIRTHKGVKIPGVIGSVAPHALTEKQKQQPLSFDEMFIDIGANSREEVEKRGVEIGNFISPEAN\\nFACWGEDKVVGKALDNRIGCAMMAELLQTVNNPEITLYGVGSVEEEVGLRGAQTSAEHIKPDVVIVLDTAVAGDVPGIDN\\nIKYPLKLGQGPGLMLFDKRYFPNQKLVAALKSCAAHNDLPLQFSTMKTGATDGGRYNVMGGGRPVVALCLPTRYLHANSG\\nMISKADYEALLTLIRGFLTTLTAEKVNAFSQFRQVD\\n>DB_ACC10 protein D (last entry)\\nMESMLDRPEQELVLVVDDTPDNLLLMRELLEEQYRVRTAGSGPAGLRAAVEEPRPDLILLDVNMPGMDGYEVCRRLKADP\\nLTRDIPLMFLTARADRDDEQQGLALGAVDYLGKPVSPPIVLARVRTHLQLKANADFLRDKSEYLELEVRRRTRQLQQLQD\\nAVIEALATLGDLRDNPRSRHLPRIERYVRLLAEHLAAQRAFADELTPEAVDLLSKSALLHDIGKVAVPDRVLLNPGQLDA\\nADTALLQGHTRAGRDALASAERRLGQPSGFLRFARQIAYSHHERWDGRGFPEGLAGERIPLAARIVALADRYDELTSRHA\\nYRPPLAHAEAVLLIQAGAGSEFDPRLVEAFVAVADAFAEVARRYADSAEALDVEMQRLEQAVAESIELTAPPA'

		"""
		#msg
		if not silent:
			print("processing keywords/accessions ...")

		#process keywords
		keywords = set(filter(None, keywords))

		#process enclosures
		if regex:
			pattern = re.compile(enclosures)
		else:
			splitters = set(list(enclosures))
			subsplitters = list(set(list(enclosures)))
			mainsplitter = subsplitters.pop()

			#check if keywords contain enclosures
			if set.intersection(splitters, set("".join(keywords))):
				sys.exit('ERROR: keywords/accessions contain enclosure characters.')

		#extract
		occurences = defaultdict(int)
		write = False
		counter = 0

		if outfilename:
			outhandle = open(outfilename, "w")
		else:
			out = []

		with open(infilename, "r") as handle:
			for line in handle:
				#process head line
				if fastatk.isHeader(line.strip()):
					write = False
					header = line
					if not regex:
						for splitter in subsplitters:
							header = header.replace(splitter, mainsplitter)
						header = header.split(mainsplitter)
						header = [x.strip('\r\n') for x in header]
						header = list(filter(None, header))
						findings = keywords.intersection(header)
					else:
						findings = keywords.intersection([x.group(1) for x in pattern.finditer(header)])

					for finding in findings:
						occurences[finding] += 1
						write = True
						counter += 1

				#write
				if write:
					if outfilename:
						outhandle.write(line)
					else:
						out.append(line)

		#findings check
		if not silent:
			print("checking output ...")

		for keyword in occurences:
			if occurences[keyword] == 0:
				sys.stderr.write("WARNING: " + keyword + " not found.\n")
			elif occurences[keyword] > 1:
				sys.stderr.write("WARNING: " + keyword + " found in multiple header.\n")

		if outfilename:
			return (counter)
		else:
			return "".join(out)


	# makeNR #
	@staticmethod
	def makeNr(filenames, outfile, soh = chr(1), seq_line_len = False, silent = False):
		'''
		removes full-length sequence redundancy from FASTA file(s) and saves a new file.
		Headers are fused using a separator of header (soh).
		Existing multiple headers are splitted based on soh and header redundancy is filtered after.
		Don't suggest to use regex hehe, it will be too time consuming for a 60GB fasta file such as NCBI's nr database!
		Sequence lines can be wrapped based on seq_line_len.

		>>> testfile = os.path.join(os.path.dirname(__file__), 'fastatk.testdata.fasta')
		>>> nrfile = fastatk.makeNr(testfile, 'test_nr.fasta.tmp', seq_line_len=80, silent=True)
		>>> with open(nrfile, 'r') as fhandle:
		... 	lines = fhandle.readlines()
		>>> 'DB_ACC10' in lines[0] and 'DB_ACC9' in lines[0]
		True
		>>> "".join(lines[1:6])
		'MESMLDRPEQELVLVVDDTPDNLLLMRELLEEQYRVRTAGSGPAGLRAAVEEPRPDLILLDVNMPGMDGYEVCRRLKADP\\nLTRDIPLMFLTARADRDDEQQGLALGAVDYLGKPVSPPIVLARVRTHLQLKANADFLRDKSEYLELEVRRRTRQLQQLQD\\nAVIEALATLGDLRDNPRSRHLPRIERYVRLLAEHLAAQRAFADELTPEAVDLLSKSALLHDIGKVAVPDRVLLNPGQLDA\\nADTALLQGHTRAGRDALASAERRLGQPSGFLRFARQIAYSHHERWDGRGFPEGLAGERIPLAARIVALADRYDELTSRHA\\nYRPPLAHAEAVLLIQAGAGSEFDPRLVEAFVAVADAFAEVARRYADSAEALDVEMQRLEQAVAESIELTAPPA\\n'
		>>> "".join(lines[6:12])
		'>DB_ACC8 protein C (entry with comment)\\nMNIELLQQLCEASAVSGDEQEVRDILINTLEPCVNEITFDGLGSFVARKGNKGPKVAVVGHMDEVGFMVTHIDESGFLRF\\nTTIGGWWNQSMLNHRVTIRTHKGVKIPGVIGSVAPHALTEKQKQQPLSFDEMFIDIGANSREEVEKRGVEIGNFISPEAN\\nFACWGEDKVVGKALDNRIGCAMMAELLQTVNNPEITLYGVGSVEEEVGLRGAQTSAEHIKPDVVIVLDTAVAGDVPGIDN\\nIKYPLKLGQGPGLMLFDKRYFPNQKLVAALKSCAAHNDLPLQFSTMKTGATDGGRYNVMGGGRPVVALCLPTRYLHANSG\\nMISKADYEALLTLIRGFLTTLTAEKVNAFSQFRQVD\\n'
		>>> "".join(lines[12:16])
		'>DB_ACC6 protein B (entry with blank line after)\\nMRYTVLIALQGALLLLLLIDDGQGQSPYPYPGMPCNSSRQCGLGTCVHSRCAHCSSDGTLCSPEDPTMVWPCCPESSCQL\\nVVGLPSLVNHYNCLPNQCTDSSQCPGGFGCMTRRSKCELCKADGEACNSPYLDWRKDKECCSGYCHTEARGLEGVCIDPK\\nKIFCTPKNPWQLAPYPPSYHQPTTLRPPTSLYDSWLMSGFLVKSTTAPSTQEEEDDY\\n'
		>>> 'DB_ACC1' in lines[16] and 'DB_ACC2' in lines[16] and 'DB_ACC4' in lines[16] and 'DB_ACC5' in lines[16]
		True
		>>> " ".split(lines[16]).count('DB_ACC4')
		0
		>>> "".join(lines[17:])
		'MTEKINKKDNYHLIFALIFLAIVSVVSMMIGSSFIPLQRVLMYFINPNDSMDQFTLEVLRLPRITLAILAGAALGMSGLM\\nLQNVLKNPIASPDIIGITGGASLSAVVFIAFFSHLTIHLLPLFAVLGGAVAMMILLVFQTKGQIRPTTLIIIGISMQTLF\\nIALVQGLLITTKQLSAAKAYTWLVGSLYGATFKDTIILGMVILAVVPLLFLVIPKMKISILDDPVAIGLGLHVQRMKLIQ\\nLITSTILVSMAISLVGNIGFVGLIAPHIAKTIVRGSYAKKLLMSAMIGAISIVIADLIGRTLFLPKEVPAGVFIAAFGAP\\nFFIYLLLTVKKL\\n'
		>>> os.remove(nrfile)

		'''

		if not isinstance(filenames, (list, tuple, set)):
			filenames = [filenames]

		if not soh:
			soh = ""

		#create file for sorting
		with tempfile.NamedTemporaryFile('r', suffix=".tmp") as sorthandle:
			fastatk.flattenFASTA (filenames, sorthandle.name, sep = '\t', silent=silent)

			entries_before = 0
			entries_after = 0
			header = 0
			with open(outfile, 'w') as outhandle:
				header = None
				seq = None
				for h, s in fastatk.readFlatFasta(sorthandle.name, silent=silent):
					entries_before += 1
					if soh != "":
						h = h[1:].split(soh) #remove > at header start
					else:
						h = [h[1:]] #remove > at header start
					if not header:
						if not silent:
							print("writing ...")
						header = h
						seq = s
					elif s == seq:
						header.extend(h)
					else:
						header = '>' + soh.join(set(header))
						seq = fastatk.breakSeq(seq, seq_line_len)
						outhandle.write(header + '\n' + seq + '\n')
						header = h
						seq = s
						entries_after += 1
				header = '>' + soh.join(set(header))
				seq = fastatk.breakSeq(seq, seq_line_len)
				outhandle.write(header + '\n' + seq + '\n')
				entries_after += 1

		if not silent:
			print(entries_before, "entries combined in", entries_after, "entries")

		return outfile

	@staticmethod
	def makeRedundant(filename, outfile, soh = chr(1), seq_line_len = False, silent = False):
		'''
		splits multiple headers in single entries
		'''
		#create file for sorting
		entries_before = 0
		entries_after = 0
		orphanes = 0
		with open(outfile, 'w') as outhandle:
			for h, s in fastatk.readFasta(filename, silent=silent):
				if not s:
					orphanes += 1
					continue
				entries_before += 1
				h = set(h[1:].split(soh)) #remove > at header start
				for header in h:
					entries_after += 1
					seq = fastatk.breakSeq(s, seq_line_len)
					outhandle.write(">" + header + '\n' + seq + '\n')

		if not silent:
			if orphanes > 0:
				m = ", " + str(orphanes) + " orphane(s) skipped"
			else:
				m = ""
			print(entries_before, "entries splitted into", entries_after, "entries" + m)

		return outfile

	# flattenFASTA #
	@staticmethod
	def flattenFASTA (infilenames, outfilename, sep = '\t', silent = False):
		if not isinstance(infilenames, (list, tuple, set)):
			infilenames = [infilenames]

		with open(outfilename, 'w') as outhandle:
			for filename in infilenames:
				orph = 0
				for header, seq in fastatk.readFasta(filename, silent = silent):
					seq = seq.replace(sep, "")
					if not seq:
						orph += 1
					else:
						outhandle.write(seq + sep + header + "\n")
				if not silent:
					if orph == 1:
						print("NOTE: 1 orphaned entry (no sequence) skipped")
					if orph > 1:
						print("NOTE: " + str(orph) + " orphaned entries (no sequence) skipped")
		return outfilename

	# flattenFASTA #
	@staticmethod
	def linFASTA (infilenames, outfilename, sep = '\n', silent = False):
		if not isinstance(infilenames, (list, tuple, set)):
			infilenames = [infilenames]

		with open(outfilename, 'w') as outhandle:
			for filename in infilenames:
				orph = 0
				for header, seq in fastatk.readFasta(filename, silent = silent):
					seq = seq.replace(sep, "")
					if not seq:
						orph += 1
					else:
						outhandle.write(header + sep + seq + "\n")
				if not silent:
					if orph == 1:
						print("NOTE: 1 orphaned entry (no sequence) skipped")
					if orph > 1:
						print("NOTE: " + str(orph) + " orphaned entries (no sequence) skipped")
		return outfilename

	@staticmethod
	def unflattenLine (line, sep = '\t'):
		line = line.strip('\r\n')
		line = line.split(sep)
		return (sep.join(line[1:]), line[0])

	@staticmethod
	def readFlatFasta (flatfile, sep = '\t', sortby_acc = False, silent = False):
		##msg
		if not silent:
			print("sorting ...")

		if sortby_acc:
			i = -1
		else:
			i = 0

		chunk_names = []

		# chunk and sort
		with open(flatfile, 'r') as input_file:
			for chunk_number in count(1):
				# read in next 50000k lines and sort them
				sorted_chunk = sorted(islice(input_file, 50000), key=lambda x: x.split(sep)[i])
				if not sorted_chunk:
					# end of input
					break

				chunk_names.append(flatfile + '.chunk{}'.format(chunk_number))
				with open(chunk_names[-1], 'w') as chunk_file:
					chunk_file.writelines(sorted_chunk)

		with ExitStack() as stack:
			files = [stack.enter_context(open(chunk)) for chunk in chunk_names]
			for line in merge(*files):
				yield fastatk.unflattenLine(line)

		for chunk in chunk_names:
			os.remove(chunk)

	# check health #
	@staticmethod
	def check_health(filename, logfile, molecule='dna', headerfmt=">\S.*", soh = chr(1), silent = False):
		'''
		checks consistency of an FASTA file and writes a log file (if not None)
		returns a tuple of number of
			entries
			header
			orphanes
			sequences
			unique sequences
			entries with illegal header format
			sequences containing illegal characters
			sequences containing ambiguous characters
			sequences containing gaps
			header assigned to same sequence multiple times
			header assigned to multiple sequences

		>>> testfile = os.path.join(os.path.dirname(__file__), 'fastatk.testdata2.fasta')
		>>> fastatk.check_health(testfile, None, 'aa', silent=True)
		(12, 16, 1, 12, 8, 1, 1, 1, 0, 2, 2)
		'''
		if molecule == 'dna':
			allwd_seq = re.compile("[ACGTWSMKRYBDHVN-]+")
			ambig_seq = re.compile("[WSMKRYBDHVN]+")
		elif molecule == 'rna':
			allwd_seq = re.compile("[ACGUWSMKRYBDHVN-]+")
			ambig_seq = re.compile("[WSMKRYBDHVN]+")
		elif molecule == 'aa':
			allwd_seq = re.compile("[ARNDCQEGHILKMFPSTWYVUOXBZJΦΩΨπζ+−-]+\*?$")
			ambig_seq = re.compile("[XBZJΦΩΨπζ+−]+")
		else:
			sys.exit("ERROR: unknown molecule type")

		headerfmt = re.compile(headerfmt)

		orphanes = set()
		headerfmt_error = set()
		entries = 0
		header = 0
		ambig = set()
		illegal = set()
		duplicates = set()
		multiples = set()
		seqs = 0
		uniq_seqs = 0
		gaps = 0

		if not silent:
			print("processing " + filename + " ...")
			print("checking entries ...")
		for h, s in fastatk.readFasta(filename, silent=True):
			if not s:
				orphanes.add(h)
			else:
				entries += 1
				header += len(h.split(soh))
			if not headerfmt.match(h):
				headerfmt_error.add(h)
			if ambig_seq.search(s):
				ambig.add(h)
			if "-" in s:
				gaps += 1
			if len(allwd_seq.sub("", s.strip().upper())) > 0:
				illegal.add(h)

		if not silent:
			print("checking duplicates ...")
		with tempfile.NamedTemporaryFile('r', suffix=".tmp") as redundant_handle:
			rfname = fastatk.makeRedundant(filename, redundant_handle.name, soh, seq_line_len=False, silent=True)
			with tempfile.NamedTemporaryFile('r', suffix=".tmp") as flat_handle:
				ffname = fastatk.flattenFASTA(rfname, flat_handle.name, sep = '\t', silent=True)
				prev_h, prev_s = None, None
				for h, s in fastatk.readFlatFasta(ffname, sortby_acc=True, silent=True):
						if h == prev_h:
							if s == prev_s:
								duplicates.add(h)
							elif s != prev_s:
								multiples.add(h)
						elif h != prev_h:
							prev_h, prev_s = h, s

		with tempfile.NamedTemporaryFile('r', suffix=".tmp") as flat_handle:
			ffname = fastatk.flattenFASTA(filename, flat_handle.name, sep = '\t', silent=True)
			prev_seq = 0
			for h, s in fastatk.readFlatFasta(ffname, sortby_acc=False, silent=True):
				if s == prev_seq:
					seqs += 1
				else:
					seqs += 1
					uniq_seqs += 1
					prev_seq = s

		out = entries, header, len(orphanes), seqs, uniq_seqs, len(headerfmt_error), len(illegal), len(ambig), gaps, len(duplicates), len(multiples)
		if logfile:
			with open(logfile, "w") as handle:
				handle.write("#entries (without orphanes): " + str(entries) + "\n")
				handle.write("#header (without orphanes): " + str(header) + "\n")
				handle.write("#orphanes: " + str(len(orphanes)) + "\n")
				handle.write("#sequences: " + str(seqs) + "\n")
				handle.write("#unique sequences: " + str(uniq_seqs) + "\n")
				handle.write("#entries with non-valid header: " + str(len(headerfmt_error)) + "\n")
				handle.write("#sequences containing illegal characters: " + str(len(illegal)) + "\n")
				handle.write("#sequences containing ambiguous characters: " + str(len(ambig)) + "\n")
				handle.write("#sequences containing gaps: " + str(gaps) + "\n")
				handle.write("#header assigned to same sequence multiple times: " + str(len(duplicates)) + "\n")
				handle.write("#header assigned to multiple sequences: " + str(len(multiples)) + "\n")

				cols = [
						["orphanes"] + sorted(orphanes),
						["non-valid header"] + sorted(headerfmt_error),
						["assigned to same sequence multiple times"] + sorted(duplicates),
						["assigned to multiple sequences"] + sorted(multiples),
						["ambiguous sequence characters"] + sorted(ambig),
						["illegal sequence characters"] + sorted(illegal),
						["orphanes"] + sorted(orphanes)
					   ]

				widths = [max([len(y) for y in x]) for x in cols]
				rows = max([len(x) for x in cols])

				for i in range(len(cols)):
					cols[i] += [''] * (rows - len(cols[i]))
					cols[i] = [x + " " * (widths[i] - len(x)) for x in cols[i]]

				for i in range(rows):
					handle.write("\t".join([x[i] for x in cols]) + "\n")

		if not silent and logfile:
			with open(logfile, "r") as handle:
				for line in handle:
					if not line.startswith("#"):
						break
					print(line.strip("\r\n #"))
				print("for more details see " + logfile)

		return out

	# digest fasta #
	@staticmethod
	def digestSeq(seq, motif_regex, mc=0, minlen=1):
		'''
		digests a given sequence (seq) using a given regexp describing the cut pattern (motif_regex: first group has to be aa immediatly before cut) allowing
		a defined number of miscleavages (mc) and returns a generator over resulting peptides of a given minimal length (minlen) providing a tuple of
		sequence, sequence start, and sequence end.

		>>> import re

		#test trypsin
		>>> regex = re.compile("[KR](?=([^P]))")
		>>> seq = "KAAAAAAAKKRKGGLLRPLL"
		>>> [x for x in fastatk.digestSeq(seq, regex)]
		[('K', 1, 1), ('AAAAAAAK', 2, 9), ('K', 10, 10), ('R', 11, 11), ('K', 12, 12), ('GGLLRPLL', 13, 20)]
		>>> [x for x in fastatk.digestSeq(seq, regex, mc=2, minlen=5)]
		[('KAAAAAAAK', 1, 9), ('KAAAAAAAKK', 1, 10), ('AAAAAAAK', 2, 9), ('AAAAAAAKK', 2, 10), ('AAAAAAAKKR', 2, 11), ('RKGGLLRPLL', 11, 20), ('KGGLLRPLL', 12, 20), ('GGLLRPLL', 13, 20)]

		#test lys-c
		>>> import re
		>>> regex = re.compile("K(?=([^P]))")
		>>> seq = "KAAAAAAAKKRKGGLLRPLL"
		>>> [x for x in fastatk.digestSeq(seq, regex)]
		[('K', 1, 1), ('AAAAAAAK', 2, 9), ('K', 10, 10), ('RK', 11, 12), ('GGLLRPLL', 13, 20)]

		#test arg-c
		>>> import re
		>>> regex = re.compile("(?:(?<=R(?=[^P]))|(?<=KK))(.)")
		>>> seq = "KAAAAKKAAAKKRKGGLLRPLL"
		>>> [x for x in fastatk.digestSeq(seq, regex)]
		[('KAAAAKK', 1, 7), ('AAAKK', 8, 12), ('R', 13, 13), ('KGGLLRPLL', 14, 22)]
		>>> [x for x in fastatk.digestSeq(seq, regex, mc=1, minlen=2)]
		[('KAAAAKK', 1, 7), ('KAAAAKKAAAKK', 1, 12), ('AAAKK', 8, 12), ('AAAKKR', 8, 13), ('RKGGLLRPLL', 13, 22), ('KGGLLRPLL', 14, 22)]

		#test arg-c
		>>> import re
		>>> regex = re.compile("(?:(?<=R(?=[^P]))|(?<=KK))(.)")
		>>> seq = "KAAAAKKAAAKKRKGGLLRPLL"
		>>> [x for x in fastatk.digestSeq(seq, regex)]
		[('KAAAAKK', 1, 7), ('AAAKK', 8, 12), ('R', 13, 13), ('KGGLLRPLL', 14, 22)]
		>>> [x for x in fastatk.digestSeq(seq, regex, mc=1, minlen=2)]
		[('KAAAAKK', 1, 7), ('KAAAAKKAAAKK', 1, 12), ('AAAKK', 8, 12), ('AAAKKR', 8, 13), ('RKGGLLRPLL', 13, 22), ('KGGLLRPLL', 14, 22)]
		>>> seq = "KKKKK"
		>>> [x for x in fastatk.digestSeq(seq, regex)]
		[('KK', 1, 2), ('K', 3, 3), ('K', 4, 4), ('K', 5, 5)]

		#test glu-c
		>>> regex = re.compile("(?<=[DE])(.)")
		>>> seq = "DAAAAAAAEDEEGGLLDPLL"
		>>> [x for x in fastatk.digestSeq(seq, regex, minlen=2)]
		[('AAAAAAAE', 2, 9), ('GGLLD', 13, 17), ('PLL', 18, 20)]

		#test asp-n
		>>> regex = re.compile(".(?=([DC]))")
		>>> seq = "AACAAAAEDEEGGLLDCCPLLD"
		>>> [x for x in fastatk.digestSeq(seq, regex)]
		[('AA', 1, 2), ('CAAAAE', 3, 8), ('DEEGGLL', 9, 15), ('D', 16, 16), ('C', 17, 17), ('CPLL', 18, 21), ('D', 22, 22)]

		'''
		s = 0
		cut_sites = [0] + [x.start(1) for x in motif_regex.finditer(seq.upper())] + [len(seq)]
		cut_sites = cut_sites[::-1]
		mc = [-x for x in range(1,mc+2)]
		while len(cut_sites) > 1:
			s = cut_sites.pop()
			l = len(cut_sites)
			for e in [cut_sites[x] for x in mc if -x <= l]:
				if e-s >= minlen:
					yield seq[s:e], s+1, e

	@staticmethod
	def digestFasta(filename, outfile, protease, mc = 0, minlen = 1, soh = chr(1), seq_line_len = False, silent = False):
		"""

		"""

		#define protease cut pattern: group 1 of regex has to be immediately after cut
		proteases = {}
		proteases["trypsin"] = "[KR](?=([^P]))"
		proteases["lys-c"] = "K(?=([^P]))"
		proteases["arg-c"] = "(?:(?<=R(?=[^P]))|(?<=KK))(.)"
		proteases["glu-c"] = "(?<=[DE])(.)"
		proteases["asp-n"] = ".(?=([DC]))"
		proteases["chymotrypsin"] = "[WYFL](?=(.))"

		#select cut pattern
		if protease not in proteases:
			sys.stderr.write("ERROR: no cut pattern defined for " + protease + ". Please select from following proteases: " + ", ".join(sorted(proteases.keys())) + "\n")

		motif_regex = re.compile(proteases[protease])

		#generate redundant pep fasta and index
		with open(outfile, 'w') as outhandle:
			p = 0
			for h, seq in fastatk.readFasta(filename, silent = silent):
				h = h[1:] #remove > at header start
				if soh:
					header = sorted(set(h.split(soh)))
				else:
					header = [h]

				for pepseq, s, e in fastatk.digestSeq(seq, motif_regex, mc=mc, minlen=minlen):
					if len(pepseq) < minlen:
						continue
					pepseq = fastatk.breakSeq(pepseq, seq_line_len)
					h = soh.join([x + " [digested][coords=" + str(s) + ":" + str(e) + "]" for x in header])
					outhandle.write(">" + h + '\n' + pepseq + '\n')
		return outfile

	@staticmethod
	def splitFasta(filename, seq_line_len=None, outdir=None, silent = False):
		"""

		"""	   
		if not os.path.isdir(outdir):
			os.mkdir(outdir)
			
		keepcharacters = (' ','.','_')
		ids=defaultdict(int)
		for h, seq in fastatk.readFasta(filename, silent = silent):
			fn = h[1:].replace(".", "_").replace("\t", " ").split(" ")[0]
			ids[fn]+=1
			if ids[fn] > 1:
				while fn + "." + str(ids[fn]) in ids:
					ids[fn]+=1
				fn = fn + "." + str(ids[fn])
			fname = "".join(c for c in fn if c.isalnum() or c in keepcharacters).strip() + ".fasta"
			if outdir:
				fname = os.path.join(outdir, fname)
			with open(fname, "w") as handle:
				handle.write(h+"\n"+fastatk.breakSeq(seq, seq_line_len)+"\n")

	@staticmethod
	def indexFasta(filename, outfasta = "indexed.fasta", oneline=False, soh = chr(1), prefix = "entry", seq_line_len = False, silent=False):
		'''
		indexes any fasta file and replaces the header by the pattern
		>prefix_index

		>>> testfile = os.path.join(os.path.dirname(__file__), 'fastatk.testdata.fasta')
		>>> fasta, index = fastatk.indexFasta(testfile, 'test_index.fasta.tmp', oneline=True, seq_line_len=80, silent=True)
		>>> with open(fasta, 'r') as fhandle:
		... 	lines = fhandle.readlines()
		>>> lines[0].strip()
		'>entry_1'
		>>> lines[28].strip()
		'>entry_6'
		>>> with open(index, 'r') as fhandle:
		... 	lines = fhandle.readlines()
		>>> lines[1].strip()
		'entry_1\\tDB_ACC1 protein A;DB_ACC2 protein A;DB_ACC4 protein A'
		>>> lines[-1].strip()
		'entry_6\\tDB_ACC10 protein D (last entry)'
		>>> os.remove(fasta)
		>>> os.remove(index)
		'''
		outindex = outfasta + '.index.tsv'
		with open(outfasta, "w") as fhandle, open(outindex, "w") as ihandle:
			ihandle.write("index\theader\n")
			if prefix:
				prefix = prefix.rstrip("_") + "_"

			e = 0
			orphanes = 0
			for h, s in fastatk.readFasta(filename, silent = silent):
				if s.strip() == "":
					orphanes += 1
					continue
				e += 1
				if soh:
					header = sorted(set(h[1:].split(soh)))
				else:
					header = [h[1:]]
				s = fastatk.breakSeq(s, seq_line_len)
				fhandle.write(">" + prefix + str(e) + "\n" + s + "\n")
				if not oneline:
					for h in header:
						ihandle.write(prefix + str(e) + "\t" + h + "\n")
				else:
					h = [x.replace(";", ",") for x in header]
					ihandle.write(prefix + str(e) + "\t" + ";".join(h) + "\n")
			if silent == False and orphanes > 0:
				print(orphanes, "orphaned entries skipped.")
		return outfasta, outindex

if __name__ == "__main__":

	version = '3.0.2'
	logsep = '=====================================\n'
	orphanefile = "orphanes.txt"

	### definition of cli arguments ###

	parser = argparse.ArgumentParser(prog='fastatk')
	subparsers = parser.add_subparsers(help='a toolbox to parse and modify FASTA files')
	subparsers.dest = 'tool'
	subparsers.required = True

	#parent parser: only single file allowed
	single_file_parser = argparse.ArgumentParser(add_help=False)
	single_file_parser.add_argument('file', metavar="FILE", help="FASTA file", type=argparse.FileType('r'))

	#parent parser: multiple files allowed
	multiple_file_parser = argparse.ArgumentParser(add_help=False)
	multiple_file_parser.add_argument('files', metavar="FILE", help="FASTA file(s)", type=argparse.FileType('r'), nargs='+')

	# create the parser for the "count" command
	parser_count = subparsers.add_parser('count', parents=[multiple_file_parser], help='counts entries in one or more FASTA files.')

	# create the parser for the "extract" command
	parser_extract = subparsers.add_parser('extract', parents=[single_file_parser], help='extracts entries of a FASTA file based on a list of keywords/accessions.')
	parser_extract.add_argument('--name', metavar='FILE', help="name of the new FASTA file containing all extracted entries. By default the output file is named 'extraction_from_fn' where fn is the name of the input FASTA file.", type=str, default=False)
	group_parser = parser_extract.add_mutually_exclusive_group()
	group_parser.add_argument('--enc', metavar='STR', help="specific characters encapsulating the keywords/accessions (joined to a string). By default greater-than (>), space, tabulator, pipe (|), and Seperator Of Header (SOH) chars are considered. This option cannot be combined with --regex", type=str, default="> \t|" + chr(1))
	group_parser.add_argument('--regex', metavar='STR', help="regular expression capturing the keywords/accessions in match group 1. This option cannot be combined with --enc", type=str)
	parser_extract.add_argument('kfile', metavar="FILE", help="plain text file containing keywords/accessions (one per line).", type=argparse.FileType('r'))

	# create the parser for the "index" command
	parser_index = subparsers.add_parser('index', parents=[single_file_parser], help='indexes FASTA entries and provides a indexed FASTA file and index file.')
	parser_index.add_argument('--name', metavar='FILE', help="name of the designated file storing the non-redundant FASTA entries (by default: indexed.fasta)", type=str, default="indexed.fasta")
	parser_index.add_argument('--prefix', metavar="INT", help="prefix used for peptide indices (default: entry_)", type=str, default='entry_')
	parser_index.add_argument('--oneline', help="use one line format for index file (header will be fused using semicolon as separator)", action='store_true')
	parser_index.add_argument('-l', metavar='INT', help="maximal length of sequence lines in the generated non-redundant FASTA file (by default: 80). If set to 0 or less, sequence lines will be not wrapped.", type=int, default=80)

	# create the parser for the "split" command
	parser_split = subparsers.add_parser('split', parents=[single_file_parser], help='splits FASTA entries in a single FASTA file to multiple FASTA files within the current directory. Be aware: existing files will be overwritten!')
	parser_split.add_argument('-o', metavar='DIR', help="name of the designated output directory (by default: current working dir)", type=str, default=None)
	parser_split.add_argument('-l', metavar='INT', help="maximal length of sequence lines in the generated FASTA files (by default: 80). If set to 0 or less, sequence lines will be not wrapped.", type=int, default=80)

	# create the parser for the "nr" command
	parser_nr = subparsers.add_parser('nr', parents=[multiple_file_parser], help='combines one or more FASTA files into a single file and removes full-length sequence redundancy')
	parser_nr.add_argument('--name', metavar='FILE', help="name of the designated file storing the non-redundant FASTA entries (by default: nr.fasta)", type=str, default="nr.fasta")
	parser_nr.add_argument('-l', metavar='INT', help="maximal length of sequence lines in the generated non-redundant FASTA file (by default: 80). If set to 0 or less, sequence lines will be not wrapped.", type=int, default=80)

	# create the parser for the "explode" command
	parser_explode = subparsers.add_parser('explode', parents=[single_file_parser], help='splits FASTA entries introduced by multiple header in one head line into single FASTA entries with single header')
	parser_explode.add_argument('--name', metavar='FILE', help="name of the designated file (by default: redundant.fasta)", type=str, default="redundant.fasta")
	parser_explode.add_argument('-l', metavar='INT', help="maximal length of sequence lines in the generated non-redundant FASTA file (by default: 80). If set to 0 or less, sequence lines will be not wrapped.", type=int, default=80)

	# create the parser for the "health" command
	parser_health = subparsers.add_parser('health', parents=[single_file_parser], help='checks consistency of a given FASTA file')
	parser_health.add_argument('--name', metavar='FILE', help="name of log file (by default: fasta_health.log)", type=str, default="fasta_health.log")
	parser_health.add_argument('--type', metavar='STR', help="sequence type (by default: aa).", choices=["dna", "rna", "aa"], default="aa")
	parser_health.add_argument('--headerfmt', metavar='STR', help="regular expression matching to valid FASTA headers (by default: >\S.*).", type=str, default=">\S.*")

	# create the parser for the "lin" command
	parser_health = subparsers.add_parser('lin', parents=[single_file_parser], help='linearize FASTA file (convert sequence lines to single lines)')
	parser_health.add_argument('--name', metavar='FILE', help="name of output file (by default: lin.log)", type=str, default="lin.fasta")

	# create the parser for the "digest" command
	parser_digest = subparsers.add_parser('digest', parents=[single_file_parser], help='creates a peptide file (FASTA) and, optionally, an index file based on a given FASTA file')
	parser_digest.add_argument('--name', metavar='FILE', help="name of the designated file containing all peptides in FASTA format (by default: peptide.fasta)", type=str, default="peptide.fasta")
	parser_digest.add_argument('--protease', help="in silico protease (default: trypsin)", choices=['trypsin', 'lys-c', 'arg-c', 'glu-c', 'asp-n', 'chymotrypsin'], default='trypsin')
	parser_digest.add_argument('--mc', metavar='INT', help="number of allowed miscleavages (default: 0)", type=int, default=0)
	parser_digest.add_argument('--minlen', metavar='INT', help="minimal length of a peptide to be considered (default: 5)", type=int, default=5)
	parser_digest.add_argument('--nr', help="combines same peptides in a single FASTA entry with a multiple header", action='store_true')
	parser_digest.add_argument('--index', help="index sequences and generate an index file combining indices with original headers", action='store_true')
	parser_digest.add_argument('--oneline', help="use one line format for index file (header will be fused using semicolon as separator)", action='store_true')
	parser_digest.add_argument('--prefix', metavar="INT", help="prefix used for peptide indices (default: pep_)", type=str, default='pep_')
	parser_digest.add_argument('-l', metavar='INT', help="maximal length of sequence lines in the generated FASTA file (by default: 80). If set to 0 or less, sequence lines will be linear and not wrapped.", type=int, default=80)

	# create the parser for the "unittest" command
	parser_unittest = subparsers.add_parser('unittest', help='performs an unittest-like routine to validate module functionality')

	#global
	parser.add_argument('--version', action='version', version='%(prog)s ' + version)
	args = parser.parse_args()

	def get_args_fname(*argv):
		fnames = []
		for handle in argv:
			fnames.append(handle.name)
			handle.close()
		return sorted(set(fnames))

	### action ###

	# tool: nr #
	if args.tool == "nr":
		filenames = get_args_fname(*args.files)
		output.check_in_out(args.name, *filenames)
		output.check_file(args.name)
		if not args.l or args.l <= 0:
			args.l = False

		fastatk.makeNr(filenames, outfile = args.name, seq_line_len = args.l)

	# tool: index #
	if args.tool == "index":
		fastafilename = get_args_fname(args.file)[0]
		output.check_in_out(args.name, fastafilename)
		output.check_file(args.name)
		if not args.l or args.l <= 0:
			args.l = False

		fastatk.indexFasta(fastafilename, outfasta = args.name, prefix = args.prefix, oneline = args.oneline, seq_line_len = args.l)

	# tool: split #
	if args.tool == "split":
		fastafilename = get_args_fname(args.file)[0]
		if not args.l or args.l <= 0:
			args.l = False

		fastatk.splitFasta(fastafilename, seq_line_len = args.l, outdir = args.o)
		
		
	# tool: explode #
	if args.tool == "explode":
		fastafilename = get_args_fname(args.file)[0]
		output.check_in_out(args.name, fastafilename)
		output.check_file(args.name)
		if not args.l or args.l <= 0:
			args.l = False

		fastatk.makeRedundant(fastafilename, outfile = args.name, seq_line_len = args.l)

	# tool: lin #
	if args.tool == "lin":
		fastafilename = get_args_fname(args.file)[0]
		output.check_in_out(args.name, fastafilename)
		output.check_file(args.name)
		fastatk.linFASTA(fastafilename, args.name, sep="\n")

	# tool: count #
	elif args.tool == "count":
		#counting
		attrs = []
		for fname in get_args_fname(*args.files):
			attr = fastatk.countEntries(fname)
			attrs.append([fname, str(attr[0]), str(attr[1]), str(attr[2])])

		#output
		print()
		h = ["FASTA file", "entries", "header", "orphanes"]
		out = [h] + sorted(attrs)
		col_widths = [ max(len(value) + 2 for value in col) for col in zip(*out) ]

		for line in out:
			print(line[0].ljust(col_widths[0]) + line[1].rjust(col_widths[1]-2) + line[2].rjust(col_widths[2]) + line[3].rjust(col_widths[3]))

	# tool: extract #
	elif args.tool == "extract":
		fastafilename = get_args_fname(args.file)[0]
		accfilename = get_args_fname(args.kfile)[0]
		output.check_in_out(fastafilename, args.name, accfilename)

		if args.name == False:
			args.name = os.path.join(os.path.dirname(fastafilename), 'extraction_from_' + os.path.basename(fastafilename))
		output.check_file(args.name)

		keywords = []
		with open(accfilename, "r") as handle:
			for line in handle:
				line = line.strip("\r\n")
				if len(line) == 0:
					continue
				keywords.append(line.strip("\r\n"))
		keywords = sorted(set(keywords))
		kcount = len(keywords)

		if kcount == 0:
			sys.exit("ERROR: no keywords found in " + accfilename)

		if args.enc:
			enc = args.enc
			regex = False
		else:
			enc = args.regex
			regex = True

		ecount = fastatk.extractEntries(keywords, enc, args.file.name, outfilename=args.name, regex = regex)

		print(ecount, 'entries were extracted based on', kcount, 'unique keywords/accessions')

	# tool: health #
	if args.tool == "health":
		fastafilename = get_args_fname(args.file)[0]
		output.check_in_out(fastafilename, args.name)
		output.check_file(args.name)
		fastatk.check_health(fastafilename, args.name, molecule=args.type, headerfmt=args.headerfmt, soh = chr(1))

	#tool digest #
	elif args.tool == "digest":
		fastafilename = get_args_fname(args.file)[0]
		outfiles = [args.name]
		indexfile = False
		if args.index:
			with tempfile.NamedTemporaryFile("r", suffix=".tmp") as handle:
				outfiles.append(handle.name)
			indexfile = args.name + ".index.txt"
		if args.nr:
			with tempfile.NamedTemporaryFile("r", suffix=".tmp") as handle:
				outfiles.append(handle.name)

		output.check_in_out(fastafilename, *set(outfiles))
		if indexfile:
			output.check_file(indexfile, *outfiles)
		else:
			output.check_file(*outfiles)

		outfile = fastatk.digestFasta(args.file.name, protease = args.protease, mc = args.mc, minlen = args.minlen, outfile = outfiles.pop())

		if args.nr:
			nrfile = fastatk.makeNr(outfile, outfile=outfiles.pop(), silent=True)
			outfile = nrfile

		if args.index:
			fastafile, indexfile = fastatk.indexFasta(outfile, outfasta=outfiles[-1], outindex=indexfile, oneline=args.oneline, prefix=args.prefix, seq_line_len=args.l, silent=True)


	# tool: unittest #
	if args.tool == "unittest":
		if not os.path.isfile(os.path.join(os.path.dirname(__file__), 'fastatk.testdata.fasta')) or not os.path.isfile(os.path.join(os.path.dirname(__file__), 'fastatk.testdata2.fasta')):
			exit("ERROR: the test file distributed with this module cannot be found.")
		out = doctest.testmod()
		print(out)
